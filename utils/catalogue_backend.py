from elg import Catalog, Entity
from tqdm import tqdm
import re
from itertools import chain

c = Catalog()


def get_id(m):
    return m.full_metadata_record.field_value.link.field_value.split("/")[-2]


def get_elg_consortium():
    project = Entity.from_id(395, display_and_stat=True)
    ids = []
    ids.append(get_id(project.record.described_entity.field_value.coordinator.field_value))
    for participant in project.record.described_entity.field_value.participating_organization.field_value:
        ids.append(get_id(participant))
    return [Entity.from_id(id, display_and_stat=True) for id in ids]


def get_elg_pilot_projects():
    c = Catalog()
    projects = c.search(entity="Project", limit=1000)
    projects = [Entity.from_id(p.id) for p in projects]

    def is_pilot_project(p):
        return (
            "european language grid" in p.record.described_entity.funding_scheme_category.lower()
            or "elg" in p.record.described_entity.funding_scheme_category.lower()
        )

    pilot_projects = [Entity.from_id(p.id, display_and_stat=True) for p in projects if is_pilot_project(p)]
    return pilot_projects


def get_created_resources(e):
    ids = []
    for rel in e.record.described_entity.field_value.reverse_relations.field_value:
        if (
            rel.field_label.en == "Funded Language Resources and Technologies"
            or rel.field_label.en == "Provided Language Resources and Technologies"
        ):
            for r in rel.field_value:
                ids.append(get_id(r))
    return ids


def get_id_to_elg_consortium_resource():
    consortium = get_elg_consortium()
    data = {}
    for partner in consortium:
        try:
            for i in get_created_resources(partner):
                data[int(i)] = partner
        except:
            print(f"ELG partner {partner.__repr__()} did not provide any resource.")
    return data


def get_id_to_elg_pilot_projects_resource():
    projects = get_elg_pilot_projects()
    data = {}
    for partner in projects:
        try:
            for i in get_created_resources(partner):
                data[int(i)] = partner
        except:
            print(f"Pilot project {partner.__repr__()} did not provide any resource.")
    return data


def get_all_entities(max):
    entities = chain.from_iterable(
        [
            c.search(entity="LanguageResource", resource="Tool/Service", limit=max),
            c.search(entity="LanguageResource", resource="Lexical/Conceptual resource", limit=max),
            c.search(entity="LanguageResource", resource="Corpus", limit=max),
            c.search(entity="LanguageResource", resource="Model", limit=max),
            c.search(entity="LanguageResource", resource="Grammar", limit=max),
            c.search(entity="LanguageResource", resource="Uncategorized Language Description", limit=max),
            c.search(entity="Organization", limit=max),
            c.search(entity="Project", limit=max),
        ]
    )
    entities_with_record = []
    for e in tqdm(entities):
        try:
            e = Entity.from_id(e.id)
            try:
                e2 = Entity.from_id(e.id, display_and_stat=True)
                e.languages = e2.languages
            except:
                print("get_all_entities exception for the languages: ", e.id)
            entities_with_record.append(e)
        except Exception as e:
            print("get_all_entities exception: ", e)
    return entities_with_record


def get_service_type(entity, type):
    if type == "ToolService":
        return re.sub(
            r"(\w)([A-Z])",
            r"\1 \2",
            entity.record["described_entity"]["lr_subclass"]["function"][0].split("/")[-1],
        )
    else:
        return ""


def get_creator_emails(entity):
    try:
        return ", ".join(set(entity.record["metadata_creator"]["email"]))
    except Exception as e:
        # print("get_creator_emails exception: ", e)
        return ""


def get_curator_emails(entity):
    try:
        return ", ".join(
            set(
                map(
                    lambda x: ", ".join(set(x["email"])),
                    entity.record["metadata_curator"],
                )
            )
        )
    except Exception as e:
        print("get_curator_emails exception: ", e)
        return ""


def get_info(entity, id_to_elg_consortium_resource, id_to_elg_pilot_projects):
    type = entity.resource_type if entity.resource_type != None else entity.record["described_entity"]["entity_type"]
    p = dict(chain.from_iterable(d.items() for d in (id_to_elg_pilot_projects, id_to_elg_consortium_resource))).get(
        entity.id
    )
    if p:
        try:
            p = p.resource_name
        except:
            p = str(p.__repr__()).replace("\n", " ")
    else:
        p = "Unknown"

    # find type of organisation
    if type == "Organization":
        legal_status = entity.record["described_entity"]["organization_legal_status"].split("/")[-1].capitalize() if isinstance(entity.record["described_entity"]["organization_legal_status"], str) else "Company"
        startup = str(bool(entity.record["described_entity"]["startup"]))
        division_category = entity.record["described_entity"]["division_category"].split("/")[-1].capitalize() if isinstance(entity.record["described_entity"]["division_category"], str) else "NotDivision"
        organization_type = legal_status if division_category == "NotDivision" else legal_status + "_" + division_category
        try:
            country = entity.record["described_entity"]["head_office_address"]["country"]
        except:
            country = "UNK"
    else:
        legal_status = None
        startup = None
        division_category = None
        country = None
        organization_type = None


    # management_object informations
    try: 
        functional_service = entity.record["management_object"]["functional_service"]
    except:
        functional_service = None
    try: 
        has_data = entity.record["management_object"]["has_data"]
    except:
        has_data = None
    try: 
        is_claimable = entity.record["management_object"]["is_claimable"]
    except:
        is_claimable = None
    try: 
        for_information_only = entity.record["management_object"]["for_information_only"]
    except:
        for_information_only = None
    try: 
        from_ele = entity.record["management_object"]["from_ele"]
    except:
        from_ele = None

    return {
        "id": entity.id,
        "type": type,
        "service_type": get_service_type(entity, type),
        "creation_date": entity.creation_date,
        "last_date_updated": entity.record["metadata_last_date_updated"],
        "creator": get_creator_emails(entity),
        "curator": get_curator_emails(entity),
        "languages": ", ".join(set(entity.languages)),
        "licences": ", ".join(set(entity.licences)),
        "views": entity.views,
        "downloads": entity.downloads,
        "provided_by_elg_consortium": True if entity.id in id_to_elg_consortium_resource.keys() else False,
        "provided_by_elg_pilot_projects": True if entity.id in id_to_elg_pilot_projects.keys() else False,
        "provided_by": p,
        "legal_status": legal_status,
        "startup": startup,
        "division_category": division_category,
        "organization_type": organization_type,
        "country": country,
        "functional_service": functional_service,    
        "has_data": has_data,
        "is_claimable": is_claimable,
        "for_information_only": for_information_only,
        "from_ele": from_ele,
    }
