import requests
import datetime
import flatdict
import os
from tqdm import tqdm

from dotenv import load_dotenv

load_dotenv()


def get_admin_bearer_token():
    data = {
        "client_id": "admin-cli",
        "username": os.getenv("KC_ADMIN_USR"),
        "password": os.getenv("KC_ADMIN_PWD"),
        "grant_type": "password",
    }
    url = "https://live.european-language-grid.eu/auth/realms/master/protocol/openid-connect/token"
    r = requests.post(url, data=data)
    r.raise_for_status()
    token = r.json()["access_token"]
    return token


def get_number_of_users(token):
    url = f"https://live.european-language-grid.eu/auth/admin/realms/ELG/users/count"
    headers = {"Authorization": f"Bearer {token}", "cache-control": "no-cache"}
    parameters = {}
    r = requests.get(url, headers=headers, params=parameters)
    r.raise_for_status()
    result = r.json()
    return result


def get_all_users(token, max=10000):
    url = f"https://live.european-language-grid.eu/auth/admin/realms/ELG/users"
    headers = {"Authorization": f"Bearer {token}", "cache-control": "no-cache"}
    parameters = {"max": max}
    r = requests.get(url, headers=headers, params=parameters)
    r.raise_for_status()
    result = r.json()
    return result


def get_user_roles(token, id):
    url = f"https://live.european-language-grid.eu/auth/admin/realms/ELG/users/{id}/role-mappings"
    headers = {"Authorization": f"Bearer {token}", "cache-control": "no-cache"}
    parameters = {}
    r = requests.get(url, headers=headers, params=parameters)
    r.raise_for_status()
    result = r.json()
    return result


def is_provider(roles, username):
    # if not isinstance(roles, list) and not isinstance(roles, dict):
    #     return False
    # if isinstance(roles, dict):
    #     if "id" in roles.keys():
    #         if roles.get("name") == "provider":
    #             return True
    #         else:
    #             return False
    #     roles = list(roles.values())
    # for role in roles:
    #     if is_provider(role):
    #         return True
    # return False
    if username == "mdel@ilsp.gr":
        return True
    return "provider" in str(roles)


def is_provider_2(roles):
    if not isinstance(roles, list) and not isinstance(roles, dict):
        return False
    if isinstance(roles, dict):
        if "id" in roles.keys():
            if roles.get("name") == "provider":
                return True
            else:
                return False
        roles = list(roles.values())
    for role in roles:
        if is_provider_2(role):
            return True
    return False
    # return "provider" in str(roles)


def get_events(token, type=None, user_id=None, max=100):
    url = f"https://live.european-language-grid.eu/auth/admin/realms/ELG/events"
    headers = {"Authorization": f"Bearer {token}", "cache-control": "no-cache"}
    parameters = {
        "type": type,
        "user": user_id,
        "max": max,
    }
    if type == None:
        parameters.pop("type")
    if user_id == None:
        parameters.pop("user")
    r = requests.get(url, headers=headers, params=parameters)
    r.raise_for_status()
    result = r.json()
    return result


def flatten(d):
    d_flat = []
    for e in tqdm(d):
        try:
            e_flat = dict(flatdict.FlatDict(e, delimiter="."))
        except:
            raise
        d_flat.append(e_flat)
    return d_flat
    # return [dict(flatdict.FlatDict(e, delimiter=".")) for e in d]


def get_datetime_from_seconds(sec):
    return datetime.datetime.fromtimestamp(sec / 1000).strftime("%d/%m/%Y")
