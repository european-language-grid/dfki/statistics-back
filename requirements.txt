fastapi>=0.68.0,<0.69.0
pydantic>=1.8.0,<2.0.0
uvicorn>=0.15.0,<0.16.0
elg @ git+https://gitlab.com/european-language-grid/platform/python-client.git@fix-missing_field
tqdm
flatdict
google-api-python-client
oauth2client
python-dotenv
tenacity