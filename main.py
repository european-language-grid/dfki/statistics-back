from typing import List, Optional
import os
import pandas as pd
import numpy as np
from pandas._libs.tslibs.timestamps import Timestamp
from pandas.tseries.offsets import DateOffset
from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
from tenacity import retry, stop_after_attempt, wait_random

from utils.catalogue_backend import (
    get_all_entities,
    get_info,
    get_id_to_elg_consortium_resource,
    get_id_to_elg_pilot_projects_resource,
)
from utils.keycloak import (
    get_admin_bearer_token,
    get_all_users,
    get_user_roles,
    is_provider,
    get_events,
    flatten,
    get_datetime_from_seconds,
)
from utils.analytics import ga_response_dataframe

from tqdm import tqdm

from fastapi import FastAPI, Query

from dotenv import load_dotenv

load_dotenv()

RESOURCES_CSV = "data/resources.csv"
USERS_CSV = "data/users.csv"
LOGIN_EVENTS_CSV = "data/login-events.csv"

# Google Analytics
SCOPES = ["https://www.googleapis.com/auth/analytics.readonly"]
KEY_FILE_LOCATION = os.getenv("KEY_FILE_LOCATION")
VIEW_ID = os.getenv("VIEW_ID")
credentials = ServiceAccountCredentials.from_json_keyfile_name(
    KEY_FILE_LOCATION, SCOPES
)
# Build the service object.
analytics = build("analyticsreporting", "v4", credentials=credentials)

COLORS = [
    "#4285f4",
    "#ea4335",
    "#fbbc04",
    "#34a853",
    "#ff6d01",
    "#46bdc6",
    "#1155cc",
    "#19a86a",
    "#d1eb60",
    "#f4aa11",
    "#a68057",
    "#f5a69b",
    "#e44819",
    "#1d2384",
]
# COLORS = [
#     "#FF7F50",
#     "#CCCCFF",
#     "#DE3163",
#     "#9FE2BF",
#     "#FFBF00",
#     "#40E0D0",
#     "#6495ED",
#     "#DFFF00",
#     "#F1414A",
#     "#62B356",
#     "#8793C0",
#     "#FFC65C",
#     "#969D80",
#     "#D406DB",
#     "#2297BF",
#     "#06EFE7",
# ]

app = FastAPI()


def order_data_function(data):
    sum_numbers = [sum(x) for x in zip(*[list(d.values()) for d in data.values()])]
    data = {
        label: {
            k: v
            for _, (k, v) in sorted(
                zip(sum_numbers, d.items()), key=lambda item: item[0], reverse=True
            )
        }
        for label, d in data.items()
    }
    return data


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/create/resources")
def create_resources():
    print("Update resources data")
    id_to_elg_consortium_resource = get_id_to_elg_consortium_resource()
    id_to_elg_pilot_projects = get_id_to_elg_pilot_projects_resource()
    entities = get_all_entities(100000)
    infos = [
        get_info(e, id_to_elg_consortium_resource, id_to_elg_pilot_projects)
        for e in tqdm(entities)
    ]
    resources = pd.DataFrame(infos)
    resources["creation_date"] = resources["creation_date"].map(
        lambda d: f"{d[-2:]}/{d[-5:-3]}/{d[:4]}"
    )
    resources["last_date_updated"] = resources["last_date_updated"].map(
        lambda d: f"{d[-2:]}/{d[-5:-3]}/{d[:4]}"
    )
    resources.to_csv(RESOURCES_CSV)
    return {"Success": True}


@app.get("/create/users")
def create_users():
    print("Update users data")
    users = flatten(get_all_users(get_admin_bearer_token(), max=1000000))
    for user in tqdm(users):
        roles = get_user_roles(get_admin_bearer_token(), user["id"])
        user["provider"] = is_provider(roles, user["username"])
    users = pd.DataFrame(users)
    users["createdTimestamp"] = users["createdTimestamp"].map(get_datetime_from_seconds)
    users.to_csv(USERS_CSV)
    return {"Success": True}


@app.get("/create/login-events")
def create_login_events():
    print("Update login events data")
    # dont go higher than 100000, otherwise the request will fail.
    login_events = pd.DataFrame(
        flatten(get_events(get_admin_bearer_token(), type="LOGIN", max=100000))
    )
    login_events["time"] = login_events["time"].map(get_datetime_from_seconds)
    login_events.to_csv(LOGIN_EVENTS_CSV)
    return {"Success": True}


@app.get("/resources/{value}")
def resources(
    value: str,
    from_date: Optional[str] = None,  # must be YYYY-MM-DD
    to_date: Optional[str] = None,  # must be YYYY-MM-DD
    types: Optional[List[str]] = Query(None),
    organization_types: Optional[List[str]] = Query(None),
    legal_status: Optional[List[str]] = Query(None),
    division_categories: Optional[List[str]] = Query(None),
    elg_integrated: Optional[bool] = None,
    to_1d: Optional[bool] = False,
    cumulate: Optional[bool] = False,
    one_dataset_per_type: Optional[bool] = False,
    one_dataset_per_organization_type: Optional[bool] = False,
    nb_of_colors: Optional[int] = -1,
    order_data: Optional[bool] = False,
    total: Optional[bool] = False,
):
    if one_dataset_per_type:
        assert types is not None
    if one_dataset_per_organization_type:
        assert organization_types is not None
    df = pd.read_csv(RESOURCES_CSV, on_bad_lines="skip")
    df["creation_date"] = pd.to_datetime(df["creation_date"], dayfirst=True)
    df["last_date_updated"] = pd.to_datetime(df["last_date_updated"], dayfirst=True)
    df["creator"] = df["creator"].apply(
        lambda s: s.split(", ") if isinstance(s, str) else s
    )
    df["curator"] = df["curator"].apply(
        lambda s: s.split(", ") if isinstance(s, str) else s
    )
    df["languages"] = df["languages"].apply(
        lambda s: s.split(", ") if isinstance(s, str) else []
    )
    df["country"] = df["country"].apply(
        lambda s: s.split(", ") if isinstance(s, str) else []
    )
    df["licences"] = df["licences"].apply(
        lambda s: s.split(", ") if isinstance(s, str) else []
    )
    if types:
        df = df[df["type"].isin(types)]
    if legal_status:
        df = df[df["legal_status"].isin(legal_status)]
    if division_categories:
        df = df[df["division_category"].isin(division_categories)]
    if elg_integrated == True:
        df = df[(df["functional_service"] == True) | (df["has_data"] == True)]
    if from_date:
        df = df[df["creation_date"] >= np.datetime64(from_date)]
    if to_date:
        df = df[df["creation_date"] < np.datetime64(to_date)]
    if not to_1d:
        if one_dataset_per_type:
            raw_data = df.value_counts([value, "type"]).to_dict()
            data = {
                timestamp: {t: 0 for t in types} for (timestamp, _) in raw_data.keys()
            }
            for k, v in raw_data.items():
                data[k[0]][k[1]] = v
            data = {
                t: {timestamp: v[t] for timestamp, v in data.items()} for t in types
            }
            if total:
                t = {}
                for d in data.values():
                    for k, v in d.items():
                        if k in t.keys():
                            t[k] += v
                        else:
                            t[k] = v

                data["Total"] = t
        elif one_dataset_per_organization_type:
            raw_data = df.value_counts([value, "organization_type"]).to_dict()
            data = {
                timestamp: {t: 0 for t in organization_types} for (timestamp, _) in raw_data.keys()
            }
            for k, v in raw_data.items():
                data[k[0]][k[1]] = v
            data = {
                t: {timestamp: v[t] for timestamp, v in data.items()} for t in legal_status
            }
            if total:
                t = {}
                for d in data.values():
                    for k, v in d.items():
                        if k in t.keys():
                            t[k] += v
                        else:
                            t[k] = v

                data["Total"] = t
        else:
            data = {value: df[value].value_counts().to_dict()}
    else:
        if one_dataset_per_type:
            values = []
            types_data = []
            for _list, t in zip(df[value], df["type"]):
                for x in _list:
                    values.append(x)
                    types_data.append(t)
            raw_data = (
                pd.DataFrame({value: values, "type": types_data})
                .value_counts([value, "type"])
                .to_dict()
            )
            data = {
                timestamp: {t: 0 for t in types} for (timestamp, _) in raw_data.keys()
            }
            for k, v in raw_data.items():
                data[k[0]][k[1]] = v
            data = {
                t: {timestamp: v[t] for timestamp, v in data.items()} for t in types
            }
        elif one_dataset_per_organization_type:
            values = []
            organization_type_data = []
            for _list, t in zip(df[value], df["organization_type"]):
                for x in _list:
                    values.append(x)
                    organization_type_data.append(t)
            raw_data = (
                pd.DataFrame({value: values, "organization_type": organization_type_data})
                .value_counts([value, "organization_type"])
                .to_dict()
            )
            data = {
                timestamp: {t: 0 for t in organization_types} for (timestamp, _) in raw_data.keys()
            }
            for k, v in raw_data.items():
                data[k[0]][k[1]] = v
            data = {
                t: {timestamp: v[t] for timestamp, v in data.items()} for t in organization_types
            }
        else:
            data = {
                value: (
                    pd.Series([x for _list in df[value] for x in _list])
                    .value_counts()
                    .to_dict()
                )
            }
    data = {label: {k: d[k] for k in sorted(d)} for label, d in data.items()}
    if order_data:
        data = order_data_function(data)
    labels = list(list(data.values())[0].keys())
    for d in data.values():
        assert list(d.keys()) == labels
    if isinstance(labels[0], Timestamp):
        labels = [l.strftime("%d/%m/%Y") for l in labels]
    datasets = {
        "labels": labels,
        "datasets": [
            {
                "label": label,
                "data": np.cumsum(list(d.values())).tolist()
                if cumulate
                else list(d.values()),
                "backgroundColor": COLORS[
                    i : len(d)
                    if nb_of_colors == -1
                    else min(len(COLORS), nb_of_colors + i)
                ],
            }
            for i, (label, d) in enumerate(data.items())
        ],
    }
    return datasets


@app.get("/users/{value}")
def users(
    value: str,
    from_date: Optional[str] = None,  # must be YYYY-MM-DD
    to_date: Optional[str] = None,  # must be YYYY-MM-DD
    cumulate: Optional[bool] = False,
    one_dataset_per_type: Optional[bool] = False,
    nb_of_colors: Optional[int] = -1,
):
    df = pd.read_csv(USERS_CSV, on_bad_lines="skip")
    df["createdTimestamp"] = pd.to_datetime(df["createdTimestamp"], dayfirst=True)
    if from_date:
        df = df[df["createdTimestamp"] >= np.datetime64(from_date)]
    if to_date:
        df = df[df["createdTimestamp"] < np.datetime64(to_date)]

    if one_dataset_per_type:
        raw_data = df.value_counts([value, "provider"]).to_dict()
        data = {
            timestamp: {t: 0 for t in [False, True]}
            for (timestamp, _) in raw_data.keys()
        }
        for k, v in raw_data.items():
            data[k[0]][k[1]] = v
        data = {
            (lambda b: "providers" if b else "consumers")(t): {
                timestamp: v[t] for timestamp, v in data.items()
            }
            for t in [False, True]
        }
    else:
        data = {value: df[value].value_counts().to_dict()}
    data = {label: {k: d[k] for k in sorted(d)} for label, d in data.items()}
    labels = list(list(data.values())[0].keys())
    for d in data.values():
        assert list(d.keys()) == labels
    if isinstance(labels[0], Timestamp):
        labels = [l.strftime("%d/%m/%Y") for l in labels]
    datasets = {
        "labels": labels,
        "datasets": [
            {
                "label": label,
                "data": np.cumsum(list(d.values())).tolist()
                if cumulate
                else list(d.values()),
                "backgroundColor": COLORS[
                    i : len(d)
                    if nb_of_colors == -1
                    else min(len(COLORS), nb_of_colors + i)
                ],
            }
            for i, (label, d) in enumerate(data.items())
        ],
    }
    return datasets


@app.get("/active-providers")
def active_providers(
    from_date: Optional[str] = None,  # must be YYYY-MM-DD
    to_date: Optional[str] = None,  # must be YYYY-MM-DD
    types: Optional[List[str]] = Query(None),
    pie: Optional[bool] = False,
    total: Optional[bool] = False,
):
    users_df = pd.read_csv(USERS_CSV, on_bad_lines="skip")
    users_df["createdTimestamp"] = pd.to_datetime(
        users_df["createdTimestamp"], dayfirst=True
    )
    if from_date:
        users_df = users_df[users_df["createdTimestamp"] >= np.datetime64(from_date)]
    if to_date:
        users_df = users_df[users_df["createdTimestamp"] < np.datetime64(to_date)]
    providers = users_df[users_df["provider"] == True]["username"].to_list()
    if "mdel@athenarc.gr" not in providers:
        providers.append("mdel@athenarc.gr")
    if "mdel@ilsp.gr" not in providers:
        providers.append("mdel@ilsp.gr")

    resources_df = pd.read_csv(RESOURCES_CSV, on_bad_lines="skip")
    resources_df["creation_date"] = pd.to_datetime(
        resources_df["creation_date"], dayfirst=True
    )
    resources_df["last_date_updated"] = pd.to_datetime(
        resources_df["last_date_updated"], dayfirst=True
    )
    resources_df["creator"] = resources_df["creator"].apply(
        lambda s: s if isinstance(s, str) else "NaN"
    )
    resources_df["curator"] = resources_df["curator"].apply(
        lambda s: s if isinstance(s, str) else "NaN"
    )
    resources_df["languages"] = resources_df["languages"].apply(
        lambda s: s.split(", ") if isinstance(s, str) else []
    )
    resources_df["licences"] = resources_df["licences"].apply(
        lambda s: s.split(", ") if isinstance(s, str) else []
    )
    if types:
        resources_df = resources_df[resources_df["type"].isin(types)]
    if from_date:
        resources_df = resources_df[
            resources_df["creation_date"] >= np.datetime64(from_date)
        ]
    if to_date:
        resources_df = resources_df[
            resources_df["creation_date"] < np.datetime64(to_date)
        ]

    def no_provider(ps, providers):
        for p in ps:
            if p in providers:
                return False
        return True

    data = {
        resource_type: {p: 0 for p in providers}
        for resource_type in set(resources_df["type"].to_list())
    }
    if total:
        data["Total"] = {p: 0 for p in providers}
    for _, row in resources_df.iterrows():
        r_creators = row["creator"].split(", ")
        r_curators = row["curator"].split(", ")
        r_providers = list(set(r_creators + r_curators))
        r_type = row["type"]
        if no_provider(r_providers, providers):
            print(
                f"===\nHmmm no provider for the resource id [{row['id']}] ({r_type})\n{r_providers}"
            )
        for p in r_providers:
            if p not in providers:
                pass
            else:
                data[r_type][p] += 1
                if total:
                    data["Total"][p] += 1
    if not pie:
        data = order_data_function(data)
        labels = list(list(data.values())[0].keys())
        for d in data.values():
            assert list(d.keys()) == labels
        datasets = {
            "labels": labels,
            "datasets": [
                {
                    "label": label,
                    "data": list(d.values()),
                    "backgroundColor": COLORS[i],
                }
                for i, (label, d) in enumerate(data.items())
            ],
        }
    else:
        datasets = {
            "labels": [
                "O resource",
                "1 resource",
                "2-5 resources",
                "6-10 resources",
                "+10 resources",
            ],
            "datasets": [
                {
                    "label": "",
                    "data": [
                        len([k for k, v in data["Total"].items() if int(v) == 0]),
                        len([k for k, v in data["Total"].items() if int(v) == 1]),
                        len([k for k, v in data["Total"].items() if int(v) >= 2])
                        - len([k for k, v in data["Total"].items() if int(v) > 5]),
                        len([k for k, v in data["Total"].items() if int(v) >= 6])
                        - len([k for k, v in data["Total"].items() if int(v) > 10]),
                        len([k for k, v in data["Total"].items() if int(v) >= 11]),
                    ],
                    "backgroundColor": COLORS[:5],
                }
            ],
        }
    return datasets


@app.get("/connections")
def connections(
    from_date: Optional[str] = None,  # must be YYYY-MM-DD
    to_date: Optional[str] = None,  # must be YYYY-MM-DD
    nb_of_colors: Optional[int] = 1,
):
    df = pd.read_csv(LOGIN_EVENTS_CSV, on_bad_lines="skip")
    df["time"] = pd.to_datetime(df["time"], dayfirst=True)
    if from_date:
        df = df[df["time"] >= np.datetime64(from_date)]
    if to_date:
        df = df[df["time"] < np.datetime64(to_date)]
    unique_users_data = df[['time', 'details.username']].groupby(df.time.dt.to_period("M")).nunique("details.username")["details.username"].to_dict()
    data = [v for k, v in sorted(unique_users_data.items())]
    labels = list(unique_users_data.keys())
    labels = [l.strftime("%m/%Y") for l in labels]
    datasets = {
        "labels": labels,
        "datasets": [
            {
                "label": "time",
                "data": data,
                "backgroundColor": COLORS[0],
            },
        ],
    }
    return datasets


@app.get("/analytics/users")
@retry(stop=stop_after_attempt(3), wait=wait_random(min=1, max=2))
def analytics_users(
    from_date: Optional[str] = None,  # must be YYYY-MM-DD
    to_date: Optional[str] = None,  # must be YYYY-MM-DD
    nb_of_colors: Optional[int] = 1,
):
    from_date = from_date if from_date else "2019-01-01"
    to_date = to_date if to_date else "2023-01-01"
    df_1dayUsers = ga_response_dataframe(
        analytics.reports()
        .batchGet(
            body={
                "reportRequests": [
                    {
                        "viewId": VIEW_ID,
                        "dateRanges": [{"startDate": from_date, "endDate": to_date}],
                        "metrics": [{"expression": "ga:1dayUsers"}],
                        "dimensions": [{"name": "ga:date"}],
                    }
                ]
            }
        )
        .execute()
    ).set_index("ga:date")
    df_7dayUsers = ga_response_dataframe(
        analytics.reports()
        .batchGet(
            body={
                "reportRequests": [
                    {
                        "viewId": VIEW_ID,
                        "dateRanges": [{"startDate": from_date, "endDate": to_date}],
                        "metrics": [{"expression": "ga:7dayUsers"}],
                        "dimensions": [{"name": "ga:date"}],
                    }
                ]
            }
        )
        .execute()
    ).set_index("ga:date")
    df_14dayUsers = ga_response_dataframe(
        analytics.reports()
        .batchGet(
            body={
                "reportRequests": [
                    {
                        "viewId": VIEW_ID,
                        "dateRanges": [{"startDate": from_date, "endDate": to_date}],
                        "metrics": [{"expression": "ga:14dayUsers"}],
                        "dimensions": [{"name": "ga:date"}],
                    }
                ]
            }
        )
        .execute()
    ).set_index("ga:date")
    df_28dayUsers = ga_response_dataframe(
        analytics.reports()
        .batchGet(
            body={
                "reportRequests": [
                    {
                        "viewId": VIEW_ID,
                        "dateRanges": [{"startDate": from_date, "endDate": to_date}],
                        "metrics": [{"expression": "ga:28dayUsers"}],
                        "dimensions": [{"name": "ga:date"}],
                    }
                ]
            }
        )
        .execute()
    ).set_index("ga:date")
    df = pd.concat(
        [df_1dayUsers, df_7dayUsers, df_14dayUsers, df_28dayUsers], axis=1
    ).dropna()
    datasets = {
        "labels": [f"{date[6:]}/{date[4:6]}/{date[:4]}" for date in df.index.tolist()],
        "datasets": [
            {
                "label": column[3:],
                "data": df[column].tolist(),
                "backgroundColor": COLORS[min(len(COLORS), nb_of_colors + i)],
                "borderColor": COLORS[min(len(COLORS), nb_of_colors + i)],
            }
            for i, column in enumerate(df.columns)
        ],
    }
    return datasets


@app.get("/analytics/users-by-months")
@retry(stop=stop_after_attempt(3), wait=wait_random(min=1, max=2))
def analytics_users_by_months(
    from_date: Optional[str] = None,  # must be YYYY-MM-DD
    to_date: Optional[str] = None,  # must be YYYY-MM-DD
    nb_of_colors: Optional[int] = 1,
):
    from_date = from_date if from_date else "2019-01-01"
    to_date = to_date if to_date else "2023-01-01"

    df = ga_response_dataframe(
        analytics.reports()
        .batchGet(
            body={
                "reportRequests": [
                    {
                        "viewId": VIEW_ID,
                        "dateRanges": [{"startDate": from_date, "endDate": to_date}],
                        "metrics": [
                            {"expression": "ga:newUsers"},
                            {"expression": "ga:users"},
                        ],
                        "dimensions": [{"name": "ga:nthMonth"}],
                    }
                ]
            }
        )
        .execute()
    ).set_index("ga:nthMonth")
    df["ga:regularUsers "] = df["ga:users"] - df["ga:newUsers"]
    df = df.drop("ga:users", axis=1)
    datasets = {
        "labels": [
            (Timestamp(from_date) + DateOffset(months=int(value))).strftime("%m/%Y")
            for value in df.index.tolist()
        ],
        "datasets": [
            {
                "label": column[3:],
                "data": df[column].tolist(),
                "backgroundColor": COLORS[min(len(COLORS), nb_of_colors + i)],
            }
            for i, column in enumerate(df.columns)
        ],
    }
    return datasets


@app.get("/analytics/pages-by-months")
@retry(stop=stop_after_attempt(3), wait=wait_random(min=1, max=2))
def analytics_users_by_months(
    from_date: Optional[str] = None,  # must be YYYY-MM-DD
    to_date: Optional[str] = None,  # must be YYYY-MM-DD
    nb_of_colors: Optional[int] = 1,
):
    from_date = from_date if from_date else "2019-01-01"
    to_date = to_date if to_date else "2023-01-01"

    df = ga_response_dataframe(
        analytics.reports()
        .batchGet(
            body={
                "reportRequests": [
                    {
                        "viewId": VIEW_ID,
                        "dateRanges": [{"startDate": from_date, "endDate": to_date}],
                        "metrics": [
                            {"expression": "ga:pageviews"},
                            {"expression": "ga:avgTimeOnPage"},
                        ],
                        "dimensions": [{"name": "ga:nthMonth"}],
                    }
                ]
            }
        )
        .execute()
    ).set_index("ga:nthMonth")
    datasets = {
        "labels": [
            (Timestamp(from_date) + DateOffset(months=int(value))).strftime("%m/%Y")
            for value in df.index.tolist()
        ],
        "datasets": [
            {
                "label": column[3:],
                "data": df[column].tolist(),
                "backgroundColor": COLORS[min(len(COLORS), nb_of_colors + i)],
                "yAxisID": "y" + str(i),
            }
            for i, column in enumerate(df.columns)
        ],
    }
    return datasets


@app.get("/analytics/pages")
@retry(stop=stop_after_attempt(3), wait=wait_random(min=1, max=2))
def analytics_users_by_months(
    from_date: Optional[str] = None,  # must be YYYY-MM-DD
    to_date: Optional[str] = None,  # must be YYYY-MM-DD
    nb_of_colors: Optional[int] = 1,
):
    from_date = from_date if from_date else "2019-01-01"
    to_date = to_date if to_date else "2023-01-01"

    df = ga_response_dataframe(
        analytics.reports()
        .batchGet(
            body={
                "reportRequests": [
                    {
                        "viewId": VIEW_ID,
                        "dateRanges": [{"startDate": from_date, "endDate": to_date}],
                        "metrics": [
                            {"expression": "ga:pageviews"},
                            {"expression": "ga:avgTimeOnPage"},
                            {"expression": "ga:exits"},
                        ],
                        "dimensions": [{"name": "ga:pagePath"}],
                        "pageSize": 10000,
                    }
                ]
            }
        )
        .execute()
    ).set_index("ga:pagePath")
    df["ga:exitsRate "] = df["ga:exits"] / df["ga:pageviews"] * 100
    df = df.drop("ga:exits", axis=1)
    df = df.sort_values(by="ga:pageviews", ascending=False)
    datasets = {
        "labels": df.index.tolist()[:100],
        "datasets": [
            {
                "label": column[3:],
                "data": df[column].tolist()[:100],
                "backgroundColor": COLORS[min(len(COLORS), nb_of_colors + i)],
                "xAxisID": "x" + str(i),
            }
            for i, column in enumerate(df.columns)
        ],
    }
    return datasets


@app.get("/analytics/sources")
@retry(stop=stop_after_attempt(3), wait=wait_random(min=1, max=2))
def analytics_users_by_months(
    from_date: Optional[str] = None,  # must be YYYY-MM-DD
    to_date: Optional[str] = None,  # must be YYYY-MM-DD
):
    from_date = from_date if from_date else "2019-01-01"
    to_date = to_date if to_date else "2023-01-01"

    df = ga_response_dataframe(
        analytics.reports()
        .batchGet(
            body={
                "reportRequests": [
                    {
                        "viewId": VIEW_ID,
                        "dateRanges": [{"startDate": from_date, "endDate": to_date}],
                        "metrics": [{"expression": "ga:newUsers"}],
                        "dimensions": [{"name": "ga:source"}],
                    }
                ]
            }
        )
        .execute()
    ).set_index("ga:source")
    df = df.sort_values(by="ga:newUsers", ascending=False)
    datasets = {
        "labels": df.index.tolist(),
        "datasets": [
            {
                "label": column[3:],
                "data": df[column].tolist(),
                "backgroundColor": COLORS[
                    : min(len(COLORS), len(list(set(df[column].tolist()))))
                ],
            }
            for i, column in enumerate(df.columns)
        ],
    }
    return datasets


@app.get("/analytics/mediums")
@retry(stop=stop_after_attempt(3), wait=wait_random(min=1, max=2))
def analytics_users_by_months(
    from_date: Optional[str] = None,  # must be YYYY-MM-DD
    to_date: Optional[str] = None,  # must be YYYY-MM-DD
):
    from_date = from_date if from_date else "2019-01-01"
    to_date = to_date if to_date else "2023-01-01"

    df = ga_response_dataframe(
        analytics.reports()
        .batchGet(
            body={
                "reportRequests": [
                    {
                        "viewId": VIEW_ID,
                        "dateRanges": [{"startDate": from_date, "endDate": to_date}],
                        "metrics": [{"expression": "ga:newUsers"}],
                        "dimensions": [{"name": "ga:medium"}],
                    }
                ]
            }
        )
        .execute()
    ).set_index("ga:medium")
    df = df.sort_values(by="ga:newUsers", ascending=False)
    datasets = {
        "labels": df.index.tolist(),
        "datasets": [
            {
                "label": column[3:],
                "data": df[column].tolist(),
                "backgroundColor": COLORS[
                    : min(len(COLORS), len(list(set(df[column].tolist()))))
                ],
            }
            for i, column in enumerate(df.columns)
        ],
    }
    return datasets


@app.get("/analytics/channels")
@retry(stop=stop_after_attempt(3), wait=wait_random(min=1, max=2))
def analytics_users_by_months(
    from_date: Optional[str] = None,  # must be YYYY-MM-DD
    to_date: Optional[str] = None,  # must be YYYY-MM-DD
):
    from_date = from_date if from_date else "2019-01-01"
    to_date = to_date if to_date else "2023-01-01"

    df = ga_response_dataframe(
        analytics.reports()
        .batchGet(
            body={
                "reportRequests": [
                    {
                        "viewId": VIEW_ID,
                        "dateRanges": [{"startDate": from_date, "endDate": to_date}],
                        "metrics": [{"expression": "ga:newUsers"}],
                        "dimensions": [{"name": "ga:channelGrouping"}],
                    }
                ]
            }
        )
        .execute()
    ).set_index("ga:channelGrouping")
    df = df.sort_values(by="ga:newUsers", ascending=False)
    datasets = {
        "labels": df.index.tolist(),
        "datasets": [
            {
                "label": column[3:],
                "data": df[column].tolist(),
                "backgroundColor": COLORS[
                    : min(len(COLORS), len(list(set(df[column].tolist()))))
                ],
            }
            for i, column in enumerate(df.columns)
        ],
    }
    return datasets
